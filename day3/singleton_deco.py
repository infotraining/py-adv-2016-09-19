def Singleton(cls):
    inst = cls()
    while True:
        yield inst

#doesnotmakesense

@Singleton
class C:
    pass

c1 = C()
c2 = C()
print((id(c1)==id(c2)))

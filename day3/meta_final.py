class final(type):
    def __new__(mcls, name, bases, dicts):
        #print("metaclass:", mcls, "name:", name, "bases:", bases)
        for base_class in bases:
            if isinstance(base_class, mcls):
                raise TypeError("class " + base_class.__name__ + " is final")
        return super().__new__(mcls, name, bases, dicts)

class A:
    pass

class B(A, metaclass=final):
    pass

class C(B):
    pass   #throws TypeError

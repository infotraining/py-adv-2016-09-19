def fibo(n):
    res = []
    a = 0
    b = 1
    for i in range(n):
        res.append(b)
        a, b = b, a
        b = a+b
    return res

class Fibo:
    class FiboIter:
        def __init__(self, limit):
            self.limit = limit
            self.a = 0
            self.b = 1

        def __next__(self):
            if self.limit > 0:
                self.a, self.b = self.b, self.a+self.b
                self.limit -= 1
                return self.a
            else:
                raise StopIteration
    
    def __init__(self, limit):
        self.limit = limit

    def __iter__(self):
        return Fibo.FiboIter(self.limit)
    

def gen_class(n):
    if n < 0:
        class C:
            pass
        return C
    else:
        class D:
            pass
        return D


C = type("C", (), {})
# C is a class

def upper(cls, bases, attrs):
    print(cls, bases, attrs)
    _attrs = { name.upper(): val for name, val in attrs.items()}
    return type(cls, bases, _attrs)

class A(metaclass=upper):
    __metaclass__ = upper
    ala = 0
    def method(self):
        pass

a = A()

class MetaClass(type):
    def __new__(mcls, name, bases, attrs):
        print("inside new", name, bases, attrs)
        return super().__new__(mcls, name, bases, attrs)

    def __init__(self, name, bases, attrs):
        print("inside init", name, bases, attrs)

    def __call__(self, *args, **kwargs):
        print("inside call", args, kwargs)
        return super().__call__(*args, **kwargs)

print("*"*40)    
class D(metaclass=MetaClass):
    def __init__(self):
        pass

    def some_fun(self):
        pass

    a = property(some_fun)




import math

def deco_map(fun):
    def wrap(arg_list):        
        for elem in arg_list:
            yield fun(elem)
    return wrap
            

@deco_map
def msqrt(n):
    return math.sqrt(n)
    

print(list(msqrt(range(10))))


def make_iter(fun, exc_class):
    try:
        while True:
            yield fun()
    except exc_class:
        pass

elements = set(range(5))

for i in make_iter(elements.pop, KeyError):
    print(i)

def gen():
    print("before")
    yield 1
    print("after")
    yield 2
    print("end")

def spell_back(word):
    for i in range(len(word)-1, -1, -1):
        yield word[i]
        
list(spell_back("Leszek"))

def fibo_gen(n):
    a, b = 0, 1
    for i in range(n):
        yield b
        a, b = b, a+b


print(list(fibo_gen(10)))

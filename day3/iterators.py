class Iterable:
    def __init__(self):
        pass

    def __iter__(self):
        return self

    def __next__(self):
        raise StopIteration

class Countdown:
    def __init__(self, start):
        self.start = start

    def __iter__(self):
        return Countdown(self.start)

    def __next__(self):
        if self.start == 0: raise StopIteration
        else:
            self.start -= 1
            return self.start

for i in Countdown(10):
    print(i)
    #10 9 8 7...

print(list(Countdown(10)))
#[10, 9, 8,...]

c = Countdown(4)
for i in c:
    for j in c:
        print(i,j)
	

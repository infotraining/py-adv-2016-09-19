
class Context:
    def __init__(self, *args):
        self.args = args

    def __enter__(self):
        print("enter")
        return self.args

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(exc_type)
        print("exit")
        if exc_type == ZeroDivisionError:
            return True
        else:
            return False

with Context("Leszek") as c:
    print(c)
    #1/0
    #d[10]

class Tag:
    def __init__(self, tag):
        self.tag = tag
        a = self.tag

    def __enter__(self):
        print("<{}>".format(self.tag, end=''))

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("</{}>".format(self.tag))

from contextlib import contextmanager

@contextmanager
def tag(t):
    try:
        print("<{}>".format(t, end=''))
        yield "as"
    finally:
        print("</{}>".format(t, end=''))

        










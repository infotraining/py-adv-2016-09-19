class autoprops(type):
    #def __new__(cls, name, bases, attrs):
    #    print(attrs)
    #    attrs["ala"] = 123
    #    print(attrs)
    #    return super().__new__(cls, name, bases, attrs)

    def __init__(self, name, bases, attrs):
        props = {}  # [foo] = (fget, fset)

        for name, func in attrs.items():
            if name.startswith("get_"):
                props.setdefault(name[4:],
                                 [None, None])[0] = func
                delattr(self, name)
                
            if name.startswith("set_"):
                props.setdefault(name[4:],
                                 [None, None])[1] = func
                delattr(self, name)            

        #print(props)
        for prop, (getter, setter) in props.items():
            setattr(self, prop, property(getter, setter))
        
              
        super().__init__(name, bases, attrs)


class MyClass(metaclass=autoprops):
    def __init__(self):
        pass

    def set_foo(self, val):
        print("setting foo to:", val)

    def get_foo(self):
        print("getting foo")

    
m = MyClass()
#m.set_foo(123)
#m.get_foo()
m.foo
m.foo = 123

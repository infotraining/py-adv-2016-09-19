import weakref

class Singleton(type):
    inst = weakref.WeakKeyDictionary()
    def __new__(mcls, name, bases, dicts):
        print("metaclass:", mcls, "name:", name, "bases:", bases)
        return super().__new__(mcls, name, bases, dicts)
    
    def __call__(cls, *args, **kwargs):
        try:
            return Singleton.inst[cls]
        except KeyError:
            Singleton.inst[cls] =  super().__call__(*args, **kwargs)
            return Singleton.inst[cls]

class C(metaclass=Singleton):
    pass

class D(C):
    pass

c1 = C()
c2 = C()
d1 = D()
d2 = D()
print(id(c1), id(c2))
print(id(d1), id(d2))
print(id(c1) == id(c2))
print(id(d1) == id(d2))

class Human:
    def __init__(self):
        self.spouse = None

Brad = Human()
Angelina = Human()
Angelina.spouse = Brad
Brad.spouse = Angelina


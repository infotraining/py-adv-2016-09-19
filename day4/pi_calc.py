import random

def in_circle(x, y):
    return True if (x**2 + y**2) < 1.0 else False

def calc_pi(n):
    hits = 0
    for i in range(n):
        x = random.uniform(-1,1)
        y = random.uniform(-1,1)
        if in_circle(x, y):
            hits +=1
    return hits/n*4

import time
from concurrent import futures

def main():
    t = time.time()
    print(calc_pi(10**6))
    print(time.time() -t)
    t = time.time()
    n = 4

    with futures.ProcessPoolExecutor(max_workers=n) as e:
        fut = []
        for i in range(n):
            fut.append(e.submit(calc_pi, int(10**6/n)))

    print(sum(f.result() for f in fut)/n)

    print(time.time() -t)

if __name__ == "__main__":
    main()

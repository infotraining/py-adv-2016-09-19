from concurrent import futures
from time import sleep
from threading import Lock

lock = Lock()

def worker(name, delay):
    for i in range(10):
        with lock:
            print(name)
        sleep(delay)
    return name + " has finished"

with futures.ThreadPoolExecutor(max_workers=4) as e:
    futuresy = []
    for i in range(1,11):
        f = e.submit(worker, "worker " + str(i), 2.0/i)
        futuresy.append(f)

    for f in futures.as_completed(futuresy):
        print(f.result())
        
##    future1 = e.submit(worker, "worker 1", 0.5)
##    future2 = e.submit(worker, "worker 2", 0.5)
##
##    print("worker 1 - running", future1.running())
##    print("worker 2 - running", future2.running())
##
##    print(future1.result())
##    print(future2.result())
    

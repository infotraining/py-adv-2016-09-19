#gcc -c primes.c
#gcc -shared primes.o -o primes.so

import ctypes

lib = ctypes.CDLL("./primes.so")
#print("4", lib.is_prime(4))
#print("97", lib.is_prime(97))

def is_prime(input):
    n = input-1
    if input <= 1: return 0
    while n > 1 :
        if input % n == 0: return 0
        n -= 1
    return 1

def primes_c_gen(n):
    for i in range(1,n):
        if lib.is_prime(i):
            yield i

def primes_gen(n):
    for i in range(1,n):
        if is_prime(i):
            yield i

def sieve(n):
    l = list(range(n))
    l[0] = False
    l[1] = False
    for i in l:        
        if i != False:            
            n = 2
            c = i*n
            while c < len(l):
                l[c] = False
                n += 1
                c = i*n
    return [p for p in l if p != False]

#print(is_prime(97))

import time
#t = time.time()
#print(sum(primes_c_gen(100000)))
#print(time.time() -t)

#t = time.time()
#print(sum(primes_gen(10000)))
#print(time.time() -t)

t = time.time()
print(sum(sieve(1000000)))
print(time.time() -t)

int is_prime(int input);

int is_prime(int input)
{
	int n = input-1;
	if (input <= 1) return 0;


	while(n > 1)
	{
		if(input % n == 0) return 0;
		n--;
	}
	return 1;
}
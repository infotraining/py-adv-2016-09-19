import threading
from contextlib import contextmanager
import time

l = 0

lock = threading.Lock()

@contextmanager
def mylock(lock):
    try:
        lock.acquire()
        yield
    finally:
        lock.release()
    

def worker_tmp(name):
    for i in range(10):
        print("worker", name, i)
        time.sleep(1)

def worker(name):
    global l
    for i in range(100000):
        #lock.acquire()
        #l += 1
        #1/0
        #lock.release()
        with lock:
            l+=1

thds = []
for i in range(4):
    thds.append(threading.Thread(target=worker,
                                 args=("worker "+str(i),)))

for th in thds:
    th.start()

print ("threads has started")

for th in thds:
    th.join()

print(l)

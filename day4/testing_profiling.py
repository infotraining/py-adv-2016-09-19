class memoize:
    def __init__(self, fun):
        self.__fun = fun
        self.__cache = {}

    def __call__(self, *args):
        if args not in self.__cache:
            self.__cache[args] = self.__fun(*args)
        return self.__cache[args]

    def __get__(self, obj, cls):
        def wrap(*args):
            return self.__call__(obj, *args)
        return wrap
        #return functools.partial(self.__call__, obj)

@memoize
def fibo(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo(n-1)+fibo(n-2)

def fibo_gen(n):
    a, b = 0, 1
    for i in range(n):
        yield b
        a, b = b, a+b


import cProfile
#cProfile.run('fibo(100000)')
cProfile.run('max(fibo_gen(100000))')



#gcc -c array.c
#gcc -shared array.o -o array.so

import ctypes

lib = ctypes.CDLL("./array.so")

import numpy

size = 10
array = numpy.ascontiguousarray(numpy.zeros(size, dtype=numpy.int16),
                                dtype=numpy.int16)

for i in array:
    print(i)


c_int16_p = ctypes.POINTER(ctypes.c_int16)
array_p = array.ctypes.data_as(c_int16_p)

lib.generate_array(array_p, size)

for i in array:
    print(i)

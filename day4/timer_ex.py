import timeit

def method1():
    out = ''
    for num in range(10000):
        out += str(num)
    return out

def method2():
    out = ''.join(str(i) for i in range(10000))
    return out


print(timeit.timeit("method1()", "from __main__ import method1", number=100))
print(timeit.timeit("method2()", "from __main__ import method2", number=100))




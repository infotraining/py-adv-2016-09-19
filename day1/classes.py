class MyClass:
    def __init__(self):
        self.myattr = 123
        self.__priv = 234

    def my_method(self, a):
        return a


class Base:
    def __init__(self):
        print("Base init")
        self.attB = 123

class D1(Base):
    def __init__(self):        
        super().__init__()
        print("D1 init")
        self.attD1 = 123

class D2(Base):
    def __init__(self):        
        super().__init__()
        print("D2 init")
        self.attD2 = 123

class Final(D1, D2):
    def __init__(self):        
        super().__init__()
        print("Final init")
        self.attF = 123


        

        

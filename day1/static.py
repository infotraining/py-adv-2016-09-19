class C:
    stat_att = 0
    def __init__(self):
        self.att = 123

    def my_fun(self, a):
        self.att += a
        C.stat_att = a

    @staticmethod
    def myadd(a, b):
        return a + b

    @classmethod
    def something(cls):
        print(cls)

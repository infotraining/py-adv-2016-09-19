class Desc:
    def __init__(self, name):
        self.name = name

    def __get__(self, inst, owner):
        print("get", inst, owner)

    def __set__(self, inst, val):
        print("set", inst, val)

class C:
    a = Desc("desktyptor")

class Property:
    def __init__(self, fget=None, fset=None):
        self.fget = fget
        self.fset = fset

    def __get__(self, inst, owner):
        if self.fget:
            return self.fget(inst)
        else:
            return None

    def __set__(self, inst, val):
        if self.fset:
            self.fset(inst, val)
        else:
            raise AttributeError
    

class Value:
    def __init__(self, v):
        self.__v = v

    def __get__val(self):
        return self.__v

    def __set__val(self, v):
        self.__v = v
    v = Property(__get__val, __set__val)

a = Value(10)
print(a.v)
a.v = 100
print(a.v)
#https://bitbucket.org/infotraining/py-adv-2016-09-19

class Access:
    def __init__(self):
        self.__val = 0        

    def __set_val(self, v):
        self.__val = v        

    def __get_val(self):
        return self.__val

    val = property(__get_val, __set_val)

    def __get_sqr(self):
        return self.val**2
    
    valsqr = property(__get_sqr)
    


    

def fun(*args, **kwargs):
    print(args, kwargs)

def fun2(user="admin", *args):
    print(user, args)

def bad_add(n, l=[]):
    l.append(n)
    return l

def add(n, l=None):
    if l is None:
        l = []
    l.append(n)
    return l

def wrap():
    count = 0
    def counter():
            nonlocal count		
            count += 1
            return count	
    return counter

def fact(n):
    if n == 1: return 1
    else:
        return fact(n-1)*n


def myfun(p):
    try:
        print(myfun.attribute)
    except AttributeError:
        myfun.attribute = p

def fibo(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo(n-1)+fibo(n-2)


def fib(n):
    try:
        fib.mem
    except AttributeError:
        fib.mem = {1:1, 2:1}

    if n < 1: return 0
    elif n not in fib.mem:
        fib.mem[n] = fib(n-1) + fib(n-2)
        return fib.mem[n]
    else:
        return fib.mem[n]
    
    

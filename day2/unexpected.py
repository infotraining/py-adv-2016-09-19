def test1():
   for i in range(5):
      def call(): return i
      yield call

def test2():
   all = []
   for i in range(5):
      def call(): return i
      all.append(call)
   return all

def test3():
   def MakeCall(i):
      def call(): return i
      return call

   all = []
   for i in range(5):
      all.append(MakeCall(i))
   return all

for test in [ test1, test2, test3 ]:
   print(test.__name__, ':', [ f() for f in test() ])

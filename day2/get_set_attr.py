class C:
    def __init__(self):
        self.attr = 123

    def __getattr__(self, name):
        pass
        #print ("accesing", name)
        #self.__dict__[name] = None

    def __setattr__(self, name, value):
        print("Setting value", name, "to", value)
        self.__dict__[name] = value
        

    def __getattribute__(self, name):
        print("getattribute", name)
        return super().__getattribute__(name)


class Enum:
    def __init__(self, *names):
        #self.__attrs = dict(zip(names, range(len(names))))
        self.__attrs = {}
        for i, name in enumerate(names,1):
            self.__attrs[name] = i

    def __getattr__(self, name):        
        if name in self.__attrs:
            return self.__attrs[name]
        else:
            raise AttributeError

    def __setattr__(self, name, value):        
        if name == "_Enum__attrs":
            super().__setattr__(name, value)
        elif name in self.__attrs:
            #super().__setattr__(name, value)
            self.__attrs[name] = value
        else:
            raise AttributeError
    
days = Enum(*"mon tue wed thu fri sat sun".split())
print(days.fri)  #5
print(days.mon)  #1
#print(days.poniedzialek) #Attribute Error

days.tue = 123 #useless...
days.poniedzialek = 234 #Attribute Error


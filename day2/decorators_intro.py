from functools import wraps

def adder(b):
    print(locals())
    def add(a):
        return a + b
    return add


def deco1(fun):
    @wraps(fun)
    def wrap(*args, **kwargs):
        print("going to execute function", fun.__name__)
        res = fun(*args, **kwargs)
        print("finished")
        return res
    return wrap


@deco1
def fun1():
    print("fun1")

#fun1 = deco1(fun1)
fun1()  ## executing wrap

@deco1
def add(a, b):
    """ adding two numbers"""
    return a+b


add(3, 4)

print("*"*40)

def cut10(fun):   #d()
    @wraps(fun)
    def wrap(*args, **kwargs):      #f'
        res = fun(*args, **kwargs)        
        return res[:10]
    return wrap   # returns f'

def cut(n): #d()
    def deco(fun):   #d'()
        @wraps(fun)
        def wrap(*args, **kwargs):      #f'
            res = fun(*args, **kwargs)        
            return res[:n]
        return wrap   # returns f'
    return deco

   

@cut(5)
def saymyname(name):
    return name

#saymyname = [cut(10)](saymyname)

print(saymyname("Leszek Tarkowski"))

class C:
    def __new__(cls, *args, **kwargs):
        print("iniside new")
        self = super().__new__(cls, *args, **kwargs)
        self.atr = 123
        return self

    def __init__(self):
        print("iniside init")
        pass


class Singleton:
    __instance = None
    def __new__(cls, *args, **kwargs):
        if cls.__instance == None:
            cls.__instance = super().__new__(cls, *args, **kwargs)        
        return cls.__instance

class MetaString(str):
    def __new__(cls, value, info):
        obj = super().__new__(cls, value)
        obj.info = info
        return obj

s1 = Singleton()
s2 = Singleton()

print(id(s1) == id(s2))

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __hash__(self):
        return self.x.__hash__() * self.y.__hash__()**2
        

class Point2:
    __slots__ = ["x", "y"]
    def __init__(self, x, y):
        self.x = x
        self.y = y
        

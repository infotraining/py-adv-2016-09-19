from functools import wraps, update_wrapper
import functools

class deco1:
    def __init__(self, fun):
        self.__fun = fun
        update_wrapper(self, fun)
    
    def __call__(self, *args, **kwargs):
        print("before call")
        res = self.__fun(*args, **kwargs)
        print("after call")
        return res

class cut:
    def __init__(self, n):
        self.__n = n

    def __call__(self, fun):
        @wraps(fun)
        def wrap(*args, **kwargs):
            return fun(*args, **kwargs)[:self.__n]
        return wrap

@deco1
def add(a, b):
    return a+b

print(add(3,4))

#@cut(10)
def printname(name):
    return name

deco = cut(10)
printname = deco(printname)

print(printname("asdasdasdasdasdasdas"))

class memoize:
    def __init__(self, fun):
        self.__fun = fun
        self.__cache = {}

    def __call__(self, *args):
        if args not in self.__cache:
            self.__cache[args] = self.__fun(*args)
        return self.__cache[args]

    def __get__(self, obj, cls):
        def wrap(*args):
            return self.__call__(obj, *args)
        return wrap
        #return functools.partial(self.__call__, obj)

@memoize
#@functools.lru_cache(maxsize=512)
def fibo(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo(n-1)+fibo(n-2)

class Fibo:
    
    @memoize
    def fibo(self, n):
        if n == 0:
            return 0
        elif n == 1:
            return 1
        else:
            return self.fibo(n-1)+self.fibo(n-2)

    #fibo = memoize(fibo)

f = Fibo()
print(f.fibo(50))

def classdeco(cls):
    orig_init = cls.__init__
    def __init__(self, *args, **kwargs):
        self.id = 123
        orig_init(self, *args, **kwargs)

    cls.__init__ = __init__
    return cls

@classdeco
class Foo:
    pass

#Foo = classdeco(Foo)

def Singleton(cls):
    inst = {}
    def wrap(*args, **kwargs):
        if cls in inst:
            return inst[cls]
        else:
            inst[cls] = cls(*args,**kwargs)
            return inst[cls]
    return wrap

def Singleton2(cls):
    orig_new = cls.__new__
    cls.__inst = None
    def __new__(cls, *args, **kwargs):
        if cls.__inst == None:
            cls.__inst = object.__new__(cls, *args, **kwargs)            
        return cls.__inst

    cls.__new__ = staticmethod(__new__)
    return cls


@Singleton2
class Bar:
    pass

b1 = Bar()
b2 = Bar()
print(id(b1) == id(b2))


class Value:
    def __init__(self, value):
        self.__value = value

    @property    
    def value(self):
        return self.__value

    @value.setter
    def value(self, value):
        self.__value = value

    #value = property(fget=value)

    

v = Value(5)
#print(v.value)
v.value = 100
#print(v.value)
